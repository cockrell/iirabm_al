import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product
import keras.backend as K
import os
from keras.models import load_model


bs = 100000
result = []
trainData = np.load('IPlist_full.npy')
for i in range(10):
    print(i)
    keras.backend.clear_session()
    filename = str('model_volume150_%s.h5' % i)
    NN = load_model(filename)
    temp = NN.predict(trainData, verbose=1, batch_size=bs)
    result.append(temp)
result = np.asarray(result)
result.shape = (10, 7**9)
np.save('Results_150.npy', result)

#
# import numpy as np
# x1=np.load('Results.npy')
# x2=np.load('Results_150.npy')
