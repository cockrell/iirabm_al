import numpy as np
import os

internalParameterization = np.empty([10000, 9], dtype=np.float32)
labels = np.empty([10000, 800], dtype=np.float32)

k = 0
for i in range(100):
    #    for j in range(500):
    file1 = str('/home/chase/iirabm_al/Run2Data/IP_1_%s.csv' % (i,j))
    file2 = str('/home/chase/iirabm_al/Run2Data/LOL_1_%s.csv' % (i))
    if os.path.isfile(file2):
        internalParameterization[k, :] = np.genfromtxt(file1, delimiter=',')
        labels[k, :] = np.genfromtxt(file2, delimiter=',')
        k = k+1

internalParameterization = internalParameterization[0:k, :]
labels = labels[0:k, :]

print(labels.shape)
print(internalParameterization.shape)

np.save('InternalParameterization_run2.npy', internalParameterization)
np.save('Labels_run2.npy', labels)
