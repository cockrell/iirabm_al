import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import os

batch_size = 100
epochs = 7500
# scoreThreshold=0.95
scoreThreshold = 1


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    model.add(Dense(4, activation='sigmoid', input_shape=(4,)))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def getArraySums(a1, a2):
    sum = 0
    for i in range(8800):
        sum = sum+abs(a1[i]-a2[i])
    return sum


def testFullData(NN, data, labels):
    PD = NN.predict(data)
    PD = np.round(PD)
    print(PD)
    labels = np.asarray(labels)
    answer = getArraySums(PD, labels)
    print("ANSWER=", answer)
    return answer


def getData():
    data = np.load('outcomes8800.npy')
    labels = data[:, 4]
    data = data[:, 0:4]
    for i in range(data.shape[0]):
        if(labels[i] > 1):
            labels[i] = 1
    print(labels.shape, data.shape)
    return data, labels


def getW(NN):
    W = np.empty(25, dtype=np.float32)
    j4 = 0
    print("TESTNN 2", NN)
    for layer in NN.layers:
        tempweight = layer.get_weights()
        for j1 in range(len(tempweight)):
            testL = np.array(tempweight[j1])
            testL = testL.ravel()
            for j2 in range(testL.shape[0]):
                W[j4] = testL[j2]
                j4 = j4+1
    W = np.asarray(W, dtype=np.float32)
#    print("W=",W)
    return W


def refineModel(x_train, x_test, y_train, y_test, indexes, data, labels):
    global scores
    global Q
    Q = Q+1
#    print("Getting Model")
    keras.backend.clear_session()
    NN = getModel()
#    print("Model Generated")
    history = NN.fit(x_train, y_train, batch_size=batch_size,
                     epochs=epochs, verbose=0)
    score = NN.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    scores.append(testFullData(NN, data, labels))
#    if((score[1]>=scoreThreshold) or (x_train.shape[0]>=8600)):
    if(Q == 25):
        W = getW(NN)
        scores = np.asarray(scores)
        np.save('scores_2.npy', scores)
        NN.save('modelTest_full2.h5')
        del NN
        keras.backend.clear_session()
#        print("TestW2",W)
        return W
    print("training set size=", x_train.shape)
    x_train, y_train, indexes = getNextSamples(
        NN, x_train, y_train, indexes, data, labels)
    print("training set size=", x_train.shape)
    tempW = refineModel(x_train, x_test, y_train,
                        y_test, indexes, data, labels)
#    print("Returning from Recursion",temp420)
    return tempW


def getNextSamples(NN, x_train, y_train, indexes, data, labels):
    probs = []
    answers = []
    temp = np.empty([len(indexes), 4], dtype=np.float32)
    for i in range(len(indexes)):
        temp[i, :] = data[indexes[i], :]
        answers.append(labels[indexes[i]])
    temp = np.asarray(temp, dtype=np.float32)
    answers = np.asarray(answers, dtype=np.float32)
    probs = NN.predict(temp)
    probs = np.asarray(probs)
    probs = np.squeeze(probs)
    p2 = probs
    p2 = np.absolute(p2-0.5)
    ind = np.argsort(p2)
    for i in range(50):
        x_train = np.vstack([x_train, data[ind[i], :]])
        y_train = np.append(y_train, labels[ind[i]])
        # the '-i' essentially re-indexes after removal of an element
        del indexes[ind[i]-i]
    return x_train, y_train, indexes


def getNNweights(data, labels):
    x_train = []
    y_train = []
    x_test = []
    y_test = []
    indexes = []
    k = 0
    for i in range(8800):
        k = k+1
        if(k % 80 == 0):
            x_train.append(data[i, :])
            y_train.append(labels[i])
        else:
            indexes.append(i)
    for i in range(100):
        temp = np.random.randint(0, high=len(indexes))
        x_test.append(data[indexes[temp], :])
        y_test.append(labels[indexes[temp]])
        del indexes[temp]
    x_train = np.asarray(x_train, dtype=np.float32)
    y_train = np.asarray(y_train, dtype=np.float32)
    x_test = np.asarray(x_test, dtype=np.float32)
    y_test = np.asarray(y_test, dtype=np.float32)
    W = refineModel(x_train, x_test, y_train, y_test, indexes, data, labels)
#    keras.backend.clear_session()
#    W=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#    print("TestW1",W)
    return W


scores = []
Q = 0
data, labels = getData()
print('Data Loaded')
tempWeight = getNNweights(data, labels)
