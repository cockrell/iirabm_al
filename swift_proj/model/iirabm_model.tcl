package require turbine 1.1.0

namespace eval iirabm_model {

    proc iirabm_model_tcl { outputs inputs args } {
      set z [ lindex $outputs 0 ]
		  set params [ lindex $inputs 0 ]
      rule $params "iirabm_model::iirabm_model_tcl_body $z $params" {*}$args type $turbine::WORK
    }

    proc iirabm_model_tcl_body { z params } {
      set params [ retrieve_string $params ]
      set z_value [ iirabm_model_run $params ]
      store_string $z $z_value
    }
}
