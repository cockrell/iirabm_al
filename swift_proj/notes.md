## Workflow Notes ##

The workflow in `swift/workflow.swift` is a nested workflow where a *sampler* resident
task provides parameters to any free *active_learning* resident tasks. The number of
*active_learning* resident tasks must be set before hand in `swift/workflow.sh` via
the *TURBINE_RESIDENT_WORK_WORKERS* variable. There must be at least 3
*TURBINE_RESIDENT_WORK_WORKERS*: one for the *sampler* resident task, one for
the *task_cache* (see below) resident task and one for an *active_learning* 
resident task. Any more than 3 and the additional resident tasks are *active_learning*
resident tasks.


The implementation consists of two nested loops driven by these resident
tasks. The overall flow looks like:

1. Initialization
2. The *sampler* produces sets of parameter
3. Each parameter set is run by the *active_learning*
4. The *active_learning* produces parameters for model runs
5. After some number of model runs, the *active_learning* returns a result to the *sampler* and we go back to step 2.

Both loops are typical EMEWS style ME loops where some python code is intialized 
with an *EQPy_init_package* and an *EQPy_run* (this latter call is new and custom 
for this). For the *sampler* we can see the initialization in lines 129 and 130. 
The *sampler* package is in `python/sampler.py` which constains some dummy code
to exercise the workflow.

The *sampler* loop starts on line 147. The *EQPy_get* on line 153 produces the actual
parameters for the active learning to work on. THe *eqpy.OUT_put* on line 19 of 
sampler.py is what is sending these parameters from *sampler.py*.

The *sampler* loops calls the active learning code in line 175 and 177.

```objc
// TODO run the NN on individual results rather than concatenating these results
string free_rank = EQPy_get(cache_loc);
results[j] = start_active_learning(p, i, j, free_rank);
```

The *EQPy_get* call get the rank of an available resident task that can
be used to run the active learning. *start_active_learning* then runs the
active learning loop on using that resident task. Note that the current code
is aggregating the dummy results from the active learning instances but 
this should be replaced with something that runs the NN on the active learning
results (or whatever is appropriate).

The placeholder active learning ME is implemented in `python/active_learning.py`. 
As usual with EMEWS and like the *sampler.py* above, this produces parameters and
passes them to swift for evaluation. The *eqpy.OUT_put(ps)* on line 32 in 
 `python/active_learning.py` produces the parameters and those parameters 
 are received by swift on line 68 in `swift/workflow.swift` in the *run_active_learning*
 loop. Note that currently the *run_model* call on line 95 that receives these parameters
 is just a placeholder. That needs to be updated with the actual code to run the model.

 There's an additional swift resident task that runs the `python/task_cache.py` package.
 This keeps track of which active learning resident tasks are available for work.
 Unfortunately, the part of the communication between the `active_learning.py` package
 and the `task_cache.py` package has to be done over MPI to work around some 
 swift-t peculiarities. 

