import io;
import sys;
import files;
import location;
import string;
import EQPy;
import python;


string emews_root = getenv("EMEWS_PROJECT_ROOT");
string turbine_output = getenv("TURBINE_OUTPUT");

string resident_work_ranks = getenv("RESIDENT_WORK_RANKS");
string r_ranks[] = split(resident_work_ranks,",");

int SAMPLER_RANK_IDX = 0;
int CACHE_RANK_IDX = 1;

// (string cls) run_model(string params, int iter, int p_num) {

//     string results[];
//     // i is used as random seed in input xml
//     foreach i in [0:trials-1:1] {
//       string instance = "%s/instance_%i_%i_%i/" % (turbine_output, iter, p_num, i+1);
//       make_dir(instance) => {
//         xml_out = instance + "config.xml";
//         //printf("params: %s", params);
//         code = to_xml_code % (params, num_threads, i, tisd, default_xml_config, xml_out);
//         file out <instance+"out.txt">;
//         file err <instance+"err.txt">;
//         python_persist(code, "'ignore'") =>
//         (out,err) = run(model_sh, xml_out, instance) =>
//         results[i] = parse_tumor_cell_count(instance);
//       }
//     }

//     string result = string_join(results, ",");
//     string code = result_template % result;
//     cls = R(code, "toString(res)");
// }

() print_time (string id) "turbine" "0.0" [
  "puts [concat \"@\" <<id>> \" time is: \" [clock milliseconds]]"
];


(string r) get_result() {
  r = "(3,10)";
}

(string result) start_active_learning(string params, int iter, int param_id, string al_rank) {
    location al_location = locationFromRank(string2int(al_rank));
    //printf(al_rank);
    EQPy_run(al_location) =>
    EQPy_put(al_location, al_rank) =>
    EQPy_put(al_location, params) =>
    run_active_learning(al_location, iter, param_id, al_rank) => 
    // get fake results from entire AL run
    result = get_result();
}

(string result) run_model(string params, int al_iter, int param_id) {
  result = params;
}

(void v) run_active_learning(location loc, int sample_iter, int param_id, string al_rank) {

    for (boolean b = true, int i = 1;
       b;
       b=c, i = i + 1)
  {
    string params =  EQPy_get(loc);
    //printf("Iter %i  next params: %s", i, params);
    // printf("AL Iter %i, %i, %i", sample_iter, param_id, i);
    //printf("al %s", params);
    boolean c;
    if (params == "DONE") {
        string final_results = EQPy_get(loc) =>
        //printf("AL final results: %s", final_results);
        v = make_void() =>
        c = false;
    } else if (params == "EQPy_ABORT") {
      printf("EQPy AL aborted: see output for Python error") =>
      string why = EQPy_get(loc);
      printf("%s", why) =>
      v = propagate() =>
      c = false;
    } else {
      string param_array[] = split(params, ";");
      string results[];
      foreach p, j in param_array
      {
          // TODO update run_model with code to actually
          // run the model with the parameters 
          // produced from the active learning.
          results[j] = run_model(p, i, j);
      }

      string res = join(results, ";");
      EQPy_put(loc, res) => c = true;
    }
  }
}

(void o) init_tasks_cache() {
  rank = r_ranks[CACHE_RANK_IDX];
  location loc = locationFromRank(string2int(rank));
  EQPy_init_package(loc, "task_cache") => 
  EQPy_run(loc) =>
  EQPy_put(loc, join(r_ranks, ",")) =>
  o = propagate();
}

(void o) init_al_rank(string rank) {
  location loc = locationFromRank(string2int(rank));
  EQPy_init_package(loc, "active_learning") =>
  EQPy_put(loc, join(r_ranks, ",")) =>
  o = propagate();
}

(string waiter[]) init_al_ranks() {
  foreach i in [2 : size(r_ranks) - 1] {
    init_al_rank(r_ranks[i]);
    waiter[i] = r_ranks[i];
  } 
}

(void o) start() {
    rank = r_ranks[SAMPLER_RANK_IDX];
    location loc = locationFromRank(string2int(rank));
    location cache_loc = locationFromRank(string2int(r_ranks[CACHE_RANK_IDX]));
    string sampler_params = "";
    EQPy_init_package(loc, "sampler") =>
    EQPy_run(loc) =>
    EQPy_put(loc, sampler_params) =>
    run_workflow(loc, cache_loc) => {
        EQPy_put(cache_loc, "DONE") =>
        o = propagate();
    }
}

(void o) run() {
    init_tasks_cache() =>
    init_al_ranks() =>
    start() =>
    o = propagate();
}

(void v) run_workflow(location sample_loc, location cache_loc) {

    for (boolean b = true, int i = 1;
       b;
       b=c, i = i + 1)
  {
    string params =  EQPy_get(sample_loc);
    //printf("Iter %i  next params: %s", i, params);
    printf("Sample Iter %i", i);
    boolean c;
    if (params == "DONE") {
        string final_results =  EQPy_get(sample_loc);
        printf(final_results);
        v = make_void() =>
        c = false;
    } else if (params == "EQPy_ABORT") {
      printf("EQPy aborted: see output for Python error") =>
      string why = EQPy_get(sample_loc);
      printf("%s", why) =>
      v = propagate() =>
      c = false;
    } else {
      string param_array[] = split(params, ";");
      string results[];
      //printf("%i", size(param_array));
      foreach p, j in param_array
      {
          // TODO run the NN on individual results rather than concatenating these results
          string free_rank = EQPy_get(cache_loc);
          //printf("free rank %s", free_rank);
          results[j] = start_active_learning(p, i, j, free_rank);
          //printf(results[j]);
      }

      string res = join(results, ";");
      EQPy_put(sample_loc, res) => c = true;
    }
  }
}

run();
