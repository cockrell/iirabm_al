import io;
import sys;
import files;
import string;

import iirabm_model;

string emews_root = getenv("EMEWS_PROJECT_ROOT");
string turbine_output = getenv("TURBINE_OUTPUT");

(void v) run_model()
{
	string param_file = argv("f"); // e.g. -f="model_params.txt"
	string param_lines[] = file_lines(input(param_file));

	string zs[];
  foreach pl,i in param_lines {
		zs[i] = iirabm_model_run(pl);
  }

	results_file = "%s/results.csv" % (turbine_output);
  file out<results_file> = write(join(zs, "\n") + "\n");
  v = propagate();
}

main {
	run_model();
}
