# NOTE THAT THIS CODE DOES NOT WORK WITH CURRENT VERIONS OF NUMPY, USE 1.14 instead
# This is due to some bug with the numpy/cytpes interface and they arent going to fix it soon
# https://github.com/numpy/numpy/pull/11277

import ctypes
import numpy as np
from mpi4py import MPI
import sys
from numpy.ctypeslib import ndpointer

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
_IIRABM = ctypes.CDLL('/home/chase/iirabm_al/IIRABM_Trajectory.so')
_IIRABM.mainSimulation.argtypes = (ctypes.c_float, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                   ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_float))
_IIRABM.mainSimulation.restype = ndpointer(
    dtype=ctypes.c_float, shape=(20, 10000))
    #(numruns*20,10000)
# _IIRABM.mainSimulation.restype=ctypes.POINTER(ctypes.c_float*1000)

# (float oxyHeal, int infectSpread,
# 	int numRecurInj, int numInfectRepeat, int inj_number, int seed, int numCytokines, float* internalParameterization){

#param combinations: {oxyHeal, infectSpread, numnRecurInj, numInfectRepeat, inj_number}
# for each combination do seed = {1,2,3}

#PBS -l nodes=10:ppn=10
#PBS -l walltime=24:00:00
#PBS -N iirabm_al
#PBS -j oe
# spack load python@2.7.14
# spack load mpich
# cd iirabm_al
# mpiexec -np 100 python fullSweepVACC.py 0


injNum = np.array([5, 10, 15, 20, 25, 30, 35, 40])
NIR = np.array([1, 2, 3, 4])
IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
NRI = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
internalParameterization = np.array(
    [1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=np.float32)

#NRI = 2
oxyHeal = np.linspace(0.05, 1, 20)

data = [0, 0, 0, 0]
index = 0
iteration = 0
for i in range(4):
    for j in range(10):
        for k in range(20):
            data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
            index = index+1
data = np.delete(data, 0, 0)
c_float_p = ctypes.POINTER(ctypes.c_float)
print("Starting IIRABM")
test = _IIRABM.mainSimulation(
    0.1, 2, 2, 2, 30, 0, 9, internalParameterization.ctypes.data_as(c_float_p))
# print(test.type)
print(test.shape)
print(test)
# X=np.asarray(test)
# print(X.shape)
# print(X)
np.save('Test.npy', test)
