import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product
import keras.backend as K
import os
from keras.models import load_model

numStochasticNetworkReplicates = 10
dataFileIndex = 4
augmentations = [0, 0.33, 0.66, 1, 2, 5, 10]
numCombos = 7**9
bs = 100000
epochs = 15000


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    # Experiment with relu
    model.add(Dense(256, activation='relu', input_shape=(9,)))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear'))
    model.summary()
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    return model


def getStartData():
    trainData = np.load('InternalParameterization_Current.npy')
    trainAnswers = np.load('TrainCenterVolumes_Current.npy')
    x_test = np.load('TestingData.npy')
    y_test = np.load('TestingAnswers.npy')
    TD2 = np.load('InternalParameterization_run%s.npy' % dataFileIndex)
    TA2 = np.load('TrainCenterVolumes_run%s.npy' % dataFileIndex)
    TD2 = TD2[0:50, :]
    trainData = np.vstack((trainData, TD2))
    trainAnswers = np.concatenate((trainAnswers, TA2[:, 0]))
    np.save('InternalParameterization_Current.npy', trainData)
    np.save('TrainCenterVolumes_Current.npy', trainAnswers)
    return trainData, trainAnswers, x_test, y_test


def generateNeuralNets(q):
    testAnswers = []
    NN = getModel()
    history = NN.fit(x_train, y_train, batch_size=x_train.shape[0],
                     epochs=epochs, verbose=0)
    score = NN.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    NN.save('model_volume%s_%s.h5' % (x_train.shape[0], q))


def getVolumeCenterPoint(data, labels):
    pt1 = []
    pt2 = []
    pt3 = []
    vol = 0
    ans1 = 0
    ans2 = 0
    ans3 = 0
    for i in range(800):
        if(labels[i] == 1):
            pt1.append(data[i, 0])
            pt2.append(data[i, 1])
            pt3.append(data[i, 2])
            vol = vol+1
    if(vol != 0):
        pt1 = np.asarray(pt1)
        pt2 = np.asarray(pt2)
        pt3 = np.asarray(pt3)
        ans1 = np.sum(pt1)/vol
        ans2 = np.sum(pt2)/vol
        ans3 = np.sum(pt3)/vol
    answer = [vol, ans1, ans2, ans3]
    return answer


fullData = np.load('IPlist_full.npy')
internalParameterization = np.empty([10000, 9], dtype=np.float32)
labels = np.empty([10000, 800], dtype=np.float32)
k = 0
for i in range(100):
    #    for j in range(500):
    file1 = str('/home/chase/iirabm_al/Run%sData/IP_%s_%s.csv' %
                (dataFileIndex, dataFileIndex, i))
    file2 = str('/home/chase/iirabm_al/Run%sData/LOL_%s_%s.csv' %
                (dataFileIndex, dataFileIndex, i))
    if os.path.isfile(file2):
        internalParameterization[k, :] = np.genfromtxt(file1, delimiter=',')
        labels[k, :] = np.genfromtxt(file2, delimiter=',')
        k = k+1
internalParameterization = internalParameterization[0:k, :]
labels = labels[0:k, :]
print(labels.shape)
print(internalParameterization.shape)
np.save('InternalParameterization_run%s.npy' %
        dataFileIndex, internalParameterization)
np.save('Labels_run%s.npy' % dataFileIndex, labels)

injNum = np.array([1, 40])
NIR = np.array([1, 2, 3, 4])
IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
NRI = 2
oxyHeal = np.linspace(0.05, 1, 20)
data = [0, 0, 0, 0]
index = 0
iteration = 0
for i in range(4):
    for j in range(10):
        for k in range(20):
            data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
            index = index+1
data = np.delete(data, 0, 0)
newData = []
for i in range(50):
    temp = getVolumeCenterPoint(data, labels[i, :])
    newData.append(temp)
newData = np.asarray(newData)
np.save('TrainCenterVolumes_run%s.npy' % dataFileIndex, newData)


x_train, y_train, x_test, y_test = getStartData()
for i in range(numStochasticNetworkReplicates):
    generateNeuralNets(i)
result = []
for i in range(10):
    print(i)
    keras.backend.clear_session()
    filename = str('model_volume%s_%s.h5' % (250, i))
    NN = load_model(filename)
    temp = NN.predict(fullData, verbose=1, batch_size=bs)
    result.append(temp)
result = np.asarray(result)
result.shape = (10, 7**9)
np.save('Results_%s.npy' % 250, result)

# result=np.load('Results_250.npy')
std = result.var(axis=0)
stdI = np.argsort(std)
stdI = np.flip(stdI)
nextData = np.zeros([100, 9], dtype=np.float32)
for i in range(100):
    nextData[i, :] = fullData[stdI[i], :]
np.save('IPlist_AL%s.npy' % (dataFileIndex+1), nextData)
