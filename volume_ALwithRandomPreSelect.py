import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product
import keras.backend as K
import os
from keras.models import load_model

numStochasticNetworkReplicates = 3
dataFileIndex = 4
augmentations = [0, 0.33, 0.66, 1, 2, 5, 10]
numCombos = 7**9
bs = 100000
epochs = 15000
numAddedPerAL = 5
numStartSamples = 10


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    # Experiment with relu
    model.add(Dense(256, activation='relu', input_shape=(9,)))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear'))
    model.summary()
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    return model


def getStartData():
    trainDataTemp = np.load('LabeledInternalParameterizations.npy')
    trainAnswersTemp = np.load('TrainCenterVolumes.npy')
    trainAnswersTemp = trainAnswersTemp[:, 0]
    x_test = trainDataTemp[4086:4186, :]
    y_test = trainAnswersTemp[4086:4186]
    trainData = np.zeros([numStartSamples, 9], dtype=np.float32)
    trainAnswers = np.zeros(numStartSamples, dtype=np.float32)
    for i in range(numStartSamples):
        temp=np.random.randint(0, 4186)
        trainData[i, :] = trainDataTemp[temp, :]
        trainAnswers[i] = trainAnswersTemp[temp]
    # trainData = trainData[0:100, :]
    # trainAnswers = trainAnswers[0:100]
    return trainData, trainAnswers, x_test, y_test


def generateNeuralNets(iter, q, x, y, xT, yT):
    testAnswers = []
    NN = getModel()
    history = NN.fit(x, y, batch_size=x.shape[0],
                     epochs=epochs, verbose=0)
    score = NN.evaluate(xT, yT, verbose=0)
    print(score)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    NN.save('model_volumeAL_start10_select5_%s_%s.h5' % (iter, q))


fullData = np.load('LabeledInternalParameterizations.npy')
fullData = fullData[0:4086, :]
fullAnswers = np.load('TrainCenterVolumes.npy')
fullAnswers = fullAnswers[0:4086, 0]
x_train, y_train, x_test, y_test = getStartData()
for AL_iter in range(300):
    for i in range(numStochasticNetworkReplicates):
        generateNeuralNets(AL_iter, i, x_train, y_train, x_test, y_test)
    result = []
    for i in range(numStochasticNetworkReplicates):
        print(i)
        keras.backend.clear_session()
        filename = str('model_volumeAL_start10_select5_%s_%s.h5' %
                       (AL_iter, i))
        NN = load_model(filename)
        temp = NN.predict(fullData, verbose=1, batch_size=10000)
        result.append(temp)
    result = np.asarray(result)
    result.shape = (numStochasticNetworkReplicates, fullData.shape[0])
    # np.save('Results_2_%s.npy'%AL_iter,result)
    # np.save('Xtrain%s'%AL_iter,x_train)
    # np.save('Ytrain%s'%AL_iter,y_train)
    std = result.var(axis=0)
    stdI = np.argsort(std)
    stdI = np.flip(stdI)
    delSelection = stdI[0:numAddedPerAL]
    nextData = np.zeros([numAddedPerAL, 9], dtype=np.float32)
    nextAnswers = np.zeros(numAddedPerAL, dtype=np.float32)
    print("FDS=", fullData.shape)
    print("NDS=", nextData.shape)
    print(stdI)
    for i in range(numAddedPerAL):
        nextData[i, :] = fullData[stdI[i], :]
        nextAnswers[i] = fullAnswers[stdI[i]]
    newData = np.delete(fullData, delSelection, axis=0)
    newAnswers = np.delete(fullAnswers, delSelection)
    fullData = newData
    fullAnswers = newAnswers
    print("FDS=", fullData.shape)
    x_train = np.vstack((x_train, nextData))
    y_train = np.concatenate((y_train, nextAnswers))
