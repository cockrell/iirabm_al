import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import os
from keras.models import load_model


def getStartData():
    IPs = np.load('InternalParameterization.npy')
    L = np.load('Labels.npy')
    X = IPs[1800:1900, :]
    Y = L[1800:1900, :]
    return X, Y


def getVolumeCenterPoint(data, labels):
    pt1 = []
    pt2 = []
    pt3 = []
    vol = 0
    ans1 = 0
    ans2 = 0
    ans3 = 0
    for i in range(800):
        if(labels[i] == 1):
            pt1.append(data[i, 0])
            pt2.append(data[i, 1])
            pt3.append(data[i, 2])
            vol = vol+1
    if(vol != 0):
        pt1 = np.asarray(pt1)
        pt2 = np.asarray(pt2)
        pt3 = np.asarray(pt3)
        ans1 = np.sum(pt1)/vol
        ans2 = np.sum(pt2)/vol
        ans3 = np.sum(pt3)/vol
    answer = [vol, ans1, ans2, ans3]
    return vol


injNum = np.array([1, 40])
NIR = np.array([1, 2, 3, 4])
IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
NRI = 2
oxyHeal = np.linspace(0.05, 1, 20)
data = [0, 0, 0, 0]
index = 0
iteration = 0
for i in range(4):
    for j in range(10):
        for k in range(20):
            data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
            index = index+1
data = np.delete(data, 0, 0)

masterModel = load_model('model1.h5')
samples, actualLabels = getStartData()
predictedVolumes = masterModel.predict(samples)
print(predictedVolumes.shape)
np.save('PredictedVolumes_1.npy', predictedVolumes)

actualVolumes = []
for i in range(100):
    actualVolumes.append(getVolumeCenterPoint(data, actualLabels[i, :]))
actualVolumes = np.asarray(actualVolumes)

np.save('ActualVolumes.npy', actualVolumes)

x = np.load('PredictedVolumes_1.npy')
y = np.load('ActualVolumes.npy')

error = []
for i in range(100):
    #    print(x[i],y[i])
    temp = abs(x[i]-y[i])/880
    print(temp)
    error.append(temp)

error = np.asarray(error)
e1 = np.mean(error)
s1 = np.std(error)

print(e1, s1)
