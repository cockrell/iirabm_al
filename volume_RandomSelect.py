7
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np

numStochasticNetworkReplicates = 3
dataFileIndex = 4
augmentations = [0, 0.33, 0.66, 1, 2, 5, 10]
numCombos = 7**9
bs = 100000
epochs = 15000
numAddedPerAL = 5
numStartSamples=10


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    # Experiment with relu
    model.add(Dense(256, activation='relu', input_shape=(9,)))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear'))
    model.summary()
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    return model


# def getStartData():
#     trainData = np.load('LabeledInternalParameterizations.npy')
#     trainAnswers = np.load('TrainCenterVolumes.npy')
#     trainAnswers = trainAnswers[:, 0]
#     x_test = trainData[4086:4186, :]
#     y_test = trainAnswers[4086:4186]
#     trainData = trainData[0:100, :]
#     trainAnswers = trainAnswers[0:100]
#     return trainData, trainAnswers, x_test, y_test

def getStartData():
    trainDataTemp = np.load('LabeledInternalParameterizations.npy')
    trainAnswersTemp = np.load('TrainCenterVolumes.npy')
    trainAnswersTemp = trainAnswersTemp[:, 0]
    x_test = trainDataTemp[4086:4186, :]
    y_test = trainAnswersTemp[4086:4186]
    trainData = np.zeros([numStartSamples, 9], dtype=np.float32)
    trainAnswers = np.zeros(numStartSamples, dtype=np.float32)
    for i in range(numStartSamples):
        temp=np.random.randint(0, 4186)
        trainData[i, :] = trainDataTemp[temp, :]
        trainAnswers[i] = trainAnswersTemp[temp]
    # trainData = trainData[0:100, :]
    # trainAnswers = trainAnswers[0:100]
    return trainData, trainAnswers, x_test, y_test


def generateNeuralNets(iter, q, x, y, xT, yT):
    testAnswers = []
    NN = getModel()
    history = NN.fit(x, y, batch_size=x.shape[0],
                     epochs=epochs, verbose=0)
    score = NN.evaluate(xT, yT, verbose=0)
    print(score)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    NN.save('model_volumeRL_start10_select5_%s_%s.h5' % (iter, q))


fullData = np.load('LabeledInternalParameterizations.npy')
#fullData = fullData[numStartSamples:4086, :]
fullAnswers = np.load('TrainCenterVolumes.npy')
fullAnswers=fullAnswers[:,0]
#fullAnswers = fullAnswers[numStartSamples:4086, 0]
x_train, y_train, x_test, y_test = getStartData()
for AL_iter in range(250):
    for i in range(numStochasticNetworkReplicates):
        generateNeuralNets(AL_iter, i, x_train, y_train, x_test, y_test)
    for j in range(numAddedPerAL):
        temp = np.random.randint(0, 4086)
        x_train = np.vstack((x_train, fullData[temp, :]))
        temp2 = fullAnswers[temp]
        y_train = np.append(y_train, temp2)
#    nextData = fullData[(AL_iter*numAddedPerAL):((AL_iter+1)*numAddedPerAL), :]
#    nextAnswers = fullAnswers[(AL_iter*numAddedPerAL):
#                              ((AL_iter+1)*numAddedPerAL)]
#    x_train = np.vstack((x_train, nextData))
#    y_train = np.concatenate((y_train, nextAnswers))
        print("XTS=", x_train.shape)
        print("YTS=", y_train.shape)
