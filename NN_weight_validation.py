import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import numpy as np
from keras.models import load_model
import os


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    model.add(Dense(4, activation='sigmoid', input_shape=(4,)))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def loadWeights(NN, input):
    L1 = np.zeros([4, 4], dtype=np.float32)
    L2 = np.zeros(4, dtype=np.float32)
    L3 = np.zeros([4, 1], dtype=np.float32)
    L4 = np.zeros([1], dtype=np.float32)
    LON1 = []
    LON2 = []
    k = 0
    for i in range(4):
        for j in range(4):
            L1[i, j] = input[k]
            k = k+1
    for i in range(4):
        L2[i] = input[k]
        k = k+1
    for i in range(4):
        L3[i, 0] = input[k]
        k = k+1
    L4 = input[k]
    L4 = np.array(L4)
    L4.shape = (1,)
    LON1.append(L1)
    LON1.append(L2)
    LON2.append(L3)
    LON2.append(L4)

    NN.layers[0].set_weights(LON1)
    NN.layers[1].set_weights(LON2)

    return NN


def getData():
    injNum = np.array([1, 40])
    NIR = np.array([1, 2, 3, 4])
    IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    NRI = 2
    oxyHeal = np.linspace(0.05, 1, 20)
    data = [0, 0, 0, 0]
    index = 0
    iteration = 0
    for i in range(4):
        for j in range(10):
            for k in range(20):
                data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
                index = index+1
    data = np.delete(data, 0, 0)
    return data


def getArraySums(a1, a2):
    sum = 0
    for i in range(800):
        sum = sum+abs(a1[i]-a2[i])
    return sum


def getStartData():
    IPs = np.load('InternalParameterization.npy')
    L = np.load('Labels.npy')
    X = IPs[1800:1900, :]
    Y = L[1800:1900, :]
    return X, Y


masterModel = load_model('NN_ParamModel_1.h5')
samples, actualLabels = getStartData()
predictedWeights = masterModel.predict(samples, verbose=1)
predictedLabels = np.empty([100, 800], dtype=np.float32)

for i in range(100):
    data = getData()
    NN = getModel()
    NN = loadWeights(NN, predictedWeights[i, :])
    temp = NN.predict(data, verbose=1)
    temp.shape = (800)
    predictedLabels[i, :] = temp

np.save('PredictedLabels.npy', predictedLabels)

PL = np.load('PredictedLabels.npy')
X = np.round(PL)
sums = []
roundSums = []
for i in range(100):
    sums.append(getArraySums(PL[i, :], actualLabels[i, :]))
    roundSums.append(getArraySums(X[i, :], actualLabels[i, :]))
sums = np.asarray(sums)
roundSums = np.asarray(roundSums)
average = np.mean(sums)
print(sums)
print(average)
print(roundSums)
print(np.mean(roundSums))
