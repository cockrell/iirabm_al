import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import numpy as np
from keras.models import load_model
import os


def getStartData():
    trainData = np.load('LabeledInternalParameterizations.npy')
    trainAnswers = np.load('TrainCenterVolumes.npy')
    trainAnswers = trainAnswers[:, 0]
    testData = trainData[4086:4186, :]
    testAnswers = trainAnswers[4086:4186]
    return trainData, trainAnswers, testData, testAnswers


#
#
fullData, fullAnswers, testData, testAnswers = getStartData()
testVolumes = np.zeros([250, 4186], dtype=np.float32)
# fullVolumes=np.zeros([60,10,4186],dtype=np.float32)
for i in range(250):
    # filename1 = str('Models_AL_Select5/model_volumeAL_start50_select5_%s_0.h5' % i)
    # filename2 = str('Models_AL_Select5/model_volumeAL_start50_select5_%s_1.h5' % i)
    # filename3 = str('Models_AL_Select5/model_volumeAL_start50_select5_%s_2.h5' % i)
    filename1 = str('Models_AL_Start10_Select5/model_volumeAL_start10_select5_%s_0.h5' % i)
    filename2 = str('Models_AL_Start10_Select5/model_volumeAL_start10_select5_%s_1.h5' % i)
    filename3 = str('Models_AL_Start10_Select5/model_volumeAL_start10_select5_%s_2.h5' % i)
    # filename4 = str('model_volumeRL_select10_%s_3.h5' % i)
    # filename5 = str('model_volumeRL_select10_%s_4.h5' % i)
    # filename6 = str('model_volumeRL_select10_%s_5.h5' % i)
    # filename7 = str('model_volumeRL_select10_%s_6.h5' % i)
    # filename8 = str('model_volumeRL_select10_%s_7.h5' % i)
    # filename9 = str('model_volumeRL_select10_%s_8.h5' % i)
    # filename10 = str('model_volumeRL_select10_%s_9.h5' % i)

    keras.backend.clear_session()
    NN = load_model(filename1)
    X1 = NN.predict(fullData)
    keras.backend.clear_session()
    NN = load_model(filename2)
    X2 = NN.predict(fullData)
    keras.backend.clear_session()
    NN = load_model(filename3)
    X3 = NN.predict(fullData)

    # keras.backend.clear_session()
    # NN = load_model(filename1)
    # X1 = NN.predict(testData)
    # keras.backend.clear_session()
    # NN = load_model(filename2)
    # X2 = NN.predict(testData)
    # keras.backend.clear_session()
    # NN = load_model(filename3)
    # X3 = NN.predict(testData)

    # keras.backend.clear_session()
    # NN = load_model(filename4)
    # X4 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename5)
    # X5 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename6)
    # X6 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename7)
    # X7 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename8)
    # X8 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename9)
    # X9 = NN.predict(testData)
    #
    # keras.backend.clear_session()
    # NN = load_model(filename10)
    # X10 = NN.predict(testData)
    # print(X1)
    # print('------------------------------------')
    # print(X2)
    # print('------------------------------------')
    # print(X3)
    # print('------------------------------------')
    # print(testAnswers)
    for j in range(4186):
        if(fullAnswers[j]==0):
            t1=0.05
            t2=0.05
            t3=0.05
        else:
#        print(fullAnswers[j])
            t1 = abs(X1[j]-fullAnswers[j])/fullAnswers[j]
            t2 = abs(X2[j]-fullAnswers[j])/fullAnswers[j]
            t3 = abs(X3[j]-fullAnswers[j])/fullAnswers[j]
        # t4 = abs(X4[j]-testAnswers[j])/testAnswers[j]
        # t5 = abs(X5[j]-testAnswers[j])/testAnswers[j]
        # t6 = abs(X6[j]-testAnswers[j])/testAnswers[j]
        # t7 = abs(X7[j]-testAnswers[j])/testAnswers[j]
        # t8 = abs(X8[j]-testAnswers[j])/testAnswers[j]
        # t9 = abs(X9[j]-testAnswers[j])/testAnswers[j]
        # t10 = abs(X10[j]-testAnswers[j])/testAnswers[j]
        # t11 = (t1+t2+t3+t3+t5+t6+t7+t8+t9+t10)/10
        t11 = (t1+t2+t3)/3
        testVolumes[i, j] = t11
    # print(testVolumes[0,:])
np.savetxt('TVA_start10_select5.csv', testVolumes, delimiter=',')
STV = np.mean(testVolumes, axis=1)
np.savetxt('STVA_start10_select5.csv', STV, delimiter=',')


#         NN=load_model(filename)
#         testResult=NN.predict(testData)
#         fullResult=NN.predict(fullData)
#         testResult=np.asarray(testResult)
#         fullResult=np.asarray(fullResult)
#         testResult.shape=(100)
#         fullResult.shape=(4186)
#         testVolumes[i,j,:]=testResult
#         fullVolumes[i,j,:]=fullResult
#
# np.save('Val_TestVol.npy',testVolumes)
# np.save('Val_FullVol.npy',fullVolumes)
#
# testVol=np.load('Val_TestVol.npy')
# fullVol=np.load('Val_FullVol.npy')
#
# testError=np.zeros([60,100],dtype=np.float32)
# fullError=np.zeros([60,4186],dtype=np.float32)
#
# for i in range(60):
#     for j in range(10):
#         for k in range(100):
#             temp=abs(testVol[i,j,k]-testAnswers[k])/testAnswers[k]
#             testError[i,k]=testError[i,k]+temp/10

# np.save('TestError',testError)
