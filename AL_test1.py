import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np

numStochasticNetworkReplicates = 10
dataFileIndex = 4
augmentations = [0, 0.33, 0.66, 1, 2, 5, 10]
numCombos = 7**9
bs = 100000
epochs = 15000


def getModel():
    keras.backend.clear_session()
    model = Sequential()
    # Experiment with relu
    model.add(Dense(256, activation='relu', input_shape=(9,)))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear'))
    model.summary()
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    return model


def getStartData():
    trainData = np.load('LabeledInternalParameterizations.npy')
    trainAnswers = np.load('TrainCenterVolumes.npy')
    trainAnswers = trainAnswers[:, 0]
    x_test = trainData[4086:4186, :]
    y_test = trainAnswers[4086:4186]
    trainData = trainData[0:2000, :]
    trainAnswers = trainAnswers[0:2000]
    return trainData, trainAnswers, x_test, y_test


trainData, trainAnswers, testData, testAnswers = getStartData()
NN = getModel()
history = NN.fit(trainData, trainAnswers, batch_size=bs,
                 epochs=epochs, verbose=0)
score = NN.evaluate(testData, testAnswers, verbose=0)
print(score)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

x3 = NN.predict(testData)
np.save('X3.npy', x3)
