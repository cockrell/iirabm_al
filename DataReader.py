# import numpy as np
# import os
#
# internalParameterization=np.empty([10000,9],dtype=np.float32)
# labels=np.empty([10000,800],dtype=np.float32)
# masterList=np.load('CurrentIPList.npy')
# size=masterList.shape[0]
#
# indexes=[]
# k=0
# for i in range(50000):
#     for j in range(200):
#         file1=str('IP_%s_%s.csv'%(i,j))
#         file2=str('LOL_%s_%s.csv'%(i,j))
#         if (os.path.isfile(file2) and os.path.isfile(file1)):
#             internalParameterization[k,:]=np.genfromtxt(file1,delimiter=',')
#             temp=internalParameterization[k,:]
#             labels[k,:]=np.genfromtxt(file2,delimiter=',')
#             k=k+1
#             index=np.where(np.all(temp==masterList,axis=1))
# #            print(index)
#             indexes.append(index)
#
# internalParameterization=internalParameterization[0:k,:]
# labels=labels[0:k,:]
#
# indexes=np.asarray(indexes)
#
#
# np.save('IP_Indexes_6',indexes)
# np.save('ListOfLabeledIPs_6.npy',internalParameterization)
# np.save('IP_Labels_6.npy',labels)


import numpy as np

masterList = np.load('CurrentIPList.npy')
Labels = np.load('MasterLabels.npy')
LabeledIPs = np.load('LabeledInternalParameterizations.npy')
newList = np.load('ListOfLabeledIPs6.npy')
newLabels = np.load('IP_Labels6.npy')
newIndexes = np.load('IP_Indexes6.npy')

print(masterList.shape)
print(Labels.shape)
print(LabeledIPs.shape)

I2 = np.sort(newIndexes)
I2 = np.flip(I2, axis=0)
I3 = I2.flatten()
Labels = np.vstack((Labels, newLabels))
LabeledIPs = np.vstack((LabeledIPs, newList))
newMasterList = np.delete(masterList, I3, axis=0)

print(newMasterList.shape)
print(Labels.shape)
print(LabeledIPs.shape)

np.save('CurrentIPList.npy', newMasterList)
np.save('LabeledInternalParameterizations.npy', LabeledIPs)
np.save('MasterLabels.npy', Labels)
