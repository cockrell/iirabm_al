import numpy as np


def getVolumeCenterPoint(data, labels):
    pt1 = []
    pt2 = []
    pt3 = []
    vol = 0
    ans1 = 0
    ans2 = 0
    ans3 = 0
    for i in range(800):
        if(labels[i] == 1):
            pt1.append(data[i, 0])
            pt2.append(data[i, 1])
            pt3.append(data[i, 2])
            vol = vol+1
    if(vol != 0):
        pt1 = np.asarray(pt1)
        pt2 = np.asarray(pt2)
        pt3 = np.asarray(pt3)
        ans1 = np.sum(pt1)/vol
        ans2 = np.sum(pt2)/vol
        ans3 = np.sum(pt3)/vol
    answer = [vol, ans1, ans2, ans3]
    return answer


IPs = np.load('LabeledInternalParameterizations.npy')
Labels = np.load('MasterLabels.npy')
numSamples = Labels.shape[0]
print("numSamples=", numSamples)

injNum = np.array([1, 40])
NIR = np.array([1, 2, 3, 4])
IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
# NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
internalParameterization = np.array(
    [1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=np.float32)

NRI = 2
oxyHeal = np.linspace(0.05, 1, 20)
data = [0, 0, 0, 0]
index = 0
iteration = 0
for i in range(4):
    for j in range(10):
        for k in range(20):
            data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
            index = index+1
data = np.delete(data, 0, 0)

trainData = []
for i in range(4186):
    temp = getVolumeCenterPoint(data, Labels[i, :])
#    print(temp)
    trainData.append(temp)
trainData = np.asarray(trainData)
np.save('TrainCenterVolumes.npy', trainData)
