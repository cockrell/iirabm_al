import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product


batch_size=100
epochs=15000
numReplicates=50
augmentations=[0,0.33,0.66,1,2,5,10]
numCombos=7**9
maxIters=10

def getModel():
    model=Sequential()
    model.add(Dense(256, activation='relu', input_shape=(9,)))  #Experiment with relu
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(25, activation='linear'))
    model.summary()
    model.compile(loss='mse',optimizer='adam',metrics=['accuracy'])
    return model

def getStartData():
    trainData=np.load('trainData.npy')
    trainAnswers=np.load('trainAnswers.npy')
    x_train=trainData[0:999,:]
    y_train=trainAnswers[0:999,:]
    x_test=trainData[1800:1899,:]
    y_test=trainAnswers[1800:1899,:]
    return x_train,y_train,x_test,y_test

def findALRanks(unlabeledIndexes,numReplicates,NN):
    data=np.empty([len(unlabeledIndexes),numReplicates],dtype=np.float32)
    for i in range(len(unlabeledIndexes)):
        for j in range(numReplicates):
            data[i,j,:]=NN.predict(listOfIPs[unlabeledIndexes[i],:])
            listOfVariances=np.empty(len(unlabeledListOfIPs),2)
    for i in range(len(unlabeledIndexes)):
        listOfVariances[i,0]=np.std(data[i,0,:])
        listOfVariances[i,1]=np.std(data[i,1,:])+np.std(data[i,1,:])+np.std(data[i,2,:])
        m1=np.amax(listOfVariances[:,0])
        m2=np.amax(listOfVariances[:,1])
        normalizedVariance=np.empty([len(unlabeledIndexes)],dtype=np.float32)
    for i in range(len(unlabeledListOfIPs)):
        normalizedVariance[i]=listOfVariances[i,0]/m1+listOfVariances[i,1]/m2
    indexes=np.argsort(normalizedVariance)
    selectedIndexes=indexes[len(unlabeledListOfIPs)-batch_size:len(unlabeledIndexes)]
    return selectedIndexes

def run():
    x_train,y_train,x_test,y_test=getStartData();
    NN=getModel()
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                    epochs=epochs,verbose=1)
    score = NN.evaluate(x_test, y_test, verbose=0)
    NN.save('NN_ParamModel_1000start_dropout.h5')
#     nextSetIndexes=findALRanks(unlabeledIndexes,numReplicates,NN)
#     for X in range(maxIters):
#         nextSet=np.empty([batch_size,9],dtype=np.float32)
#         for i in range(batch_size):
#             nextSet[i,:]=unlabeledListOfIPs[nextSetIndexes,:]
# #        nextSetLabels=CALL_ACTIVE_LEARNING(nextSet)
#         for i in range(1000):
#             labeledListOfIPs.append(nextSetIndexes[i])
#             labels.append(nextSetLabels[i])
#             unlabeledIndexes.remove(nextSetLabels[i])
#         x_train=np.vstack(x_train,nextSet[0:899,:])
#         y_train=np.vstack(x_train,nextSetLabels[0:899])
#         x_test=np.vstack(x_test,nextSet[900:999,:])
#         y_test=np.vstack(x_test,nextSetLabels[900:999])
#         history = NN.fit(x_train, y_train,batch_size=batch_size,
#                         epochs=epochs,verbose=1)
#         score = NN.evaluate(x_test, y_test, verbose=0)
#         nextSetIndexes=findALRanks(unlabeledListOfIPs,numReplicates,NN)
run()
# print('Test loss:', score[0])
# print('Test accuracy:', score[1])
