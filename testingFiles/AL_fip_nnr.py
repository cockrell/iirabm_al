import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import numpy as np
from itertools import product

batch_size=100
epochs=20000

def getModel():
    model=Sequential()
    adad=keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
    model.add(Dense(4, activation='sigmoid', input_shape=(3,)))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy',optimizer='adam'  ,metrics=['accuracy'])
    return model

def run():
    print("Getting Model")
    NN=getModel()
    print("Model Generated")
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                     epochs=epochs,verbose=1)
    score = NN.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
#    for layer in NN.layers: print(layer.get_config(), layer.get_weights())
    for layer in NN.layers: print(layer.get_weights())

#    for i in range(100):
#        print(x_test[i,:])
    tempAnswer=NN.predict(x_test)
    compareArray=np.empty(100,dtype=np.float32)
    sum=0
    for i in range(100):
        if(tempAnswer[i]<=0.5):
            compareArray[i]=0
        else:
            compareArray[i]=1
        sum=sum+(abs(compareArray[i]-y_test[i]))
#        print(y_test[i],compareArray[i])
    print(sum)











injNum=np.array([1,40])
NIR=np.array([1,2,3,4])
IS=np.array([1,2,3,4,5,6,7,8,9,10])
NRI=2
oxyHeal=np.linspace(0.05,1,20)
data=[0,0,0,0]
index=0;
iteration=0;
for i in range(4):
    for j in range(10):
        for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1

data=np.delete(data,0,0)
print(data)
labels=np.genfromtxt('LOL.csv',delimiter=',')
indexes=[]
x_train=[]
y_train=[]
x_test=[]
y_test=[]
print("Generating Indexes")
for i in range(800):
    indexes.append(i)
# while(len(indexes)>0):
#     temp=np.random.randint(0,len(indexes))
#     if(len(x_train)<700):
#         x_train.append(data[indexes[temp],:])
#         y_train.append(labels[indexes[temp]])
#     else:
#         x_test.append(data[indexes[temp],:])
#         y_test.append(labels[indexes[temp]])
#     del indexes[temp]
k=0
for i in range(800):
    k=k+1
    if(k%4==0):
        x_train.append(data[i,:])
        y_train.append(labels[i])
for i in range(100):
    temp=np.random.randint(0,high=800)
    x_test.append(data[indexes[temp],:])
    y_test.append(labels[indexes[temp]])
x_train=np.asarray(x_train,dtype=np.float32)
y_train=np.asarray(y_train,dtype=np.float32)

# x_train=x_train[0:200,:]
# y_train=y_train[0:200]
2
x_test=np.asarray(x_test,dtype=np.float32)
y_test=np.asarray(y_test,dtype=np.float32)
y_binary = to_categorical(y_train)
y_binaryTest = to_categorical(y_test)




print("Shapes:",x_train.shape,y_train.shape,x_test.shape,y_test.shape)
run()
