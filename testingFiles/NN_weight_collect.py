import numpy as np

startData=np.empty([1900,34],dtype=np.float32)
for i in range(50):
    filename=str('NNWeights/NN_Weights_%s.npy'%i)
    temp=np.load(filename)
    startData[i*38:(i+1)*38,:]=temp

np.save('trainData.npy',startData[:,0:9])
np.save('trainAnswers.npy',startData[:,9:35])
