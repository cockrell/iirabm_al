import ctypes
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from mpi4py import MPI
from sklearn.model_selection import cross_val_score
import pickle

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
_IIRABM=ctypes.CDLL('/home/chase/iirabm_al/testABM.so')
_IIRABM.mainSimulation.argtypes=(ctypes.c_float,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.POINTER(ctypes.c_float))

injNum=np.array([1,40])
NIR=np.array([1,2,3,4])
IS=np.array([1,2,3,4,5,6,7,8,9,10])
# NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
internalParameterization=np.array([1,1,1,1,1,1,1,1,1],dtype=np.float32)

NRI=2
oxyHeal=np.linspace(0.05,1,20)

data=[0,0,0,0]
index=0;
iteration=0;
for i in range(4):
    for j in range(10):
        for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1
data=np.delete(data,0,0)

def getClass(NIR,IS,NRI,oxyHeal,internalParameterization):
    answer=0
    c_float_p = ctypes.POINTER(ctypes.c_float)
    NIR=ctypes.c_int(NIR)
    IS=ctypes.c_int(IS)
    NRI=ctypes.c_int(NRI)
    oxyHeal=ctypes.c_float(oxyHeal)
    answer=_IIRABM.mainSimulation(oxyHeal,IS,NRI,NIR,9,internalParameterization.ctypes.data_as(c_float_p))
#    print(color,newRank,answer)
#    print(oxyHeal,IS,NIR)
    return answer

#    print(rank,"IterateTest")

if rank==0:
    print("Starting Iteration")
#    print(data[recvbuf,0],data[recvbuf,1],data[recvbuf,3],data[recvbuf,2],internalParameterization)
listOfLabels=[]

for count in range(8):
    comm.Barrier()
    answer=getClass(data[rank+count*size,0],data[rank+count*size,1],data[rank+count*size,3],data[rank+count*size,2],internalParameterization)
    recvbuf=None
    sendbuf=np.empty(1,dtype=np.int64)
    sendbuf[0]=answer
    if rank==0:
        recvbuf=np.empty([size],dtype=np.int64)
    comm.Gather(sendbuf,recvbuf,root=0)
    if(rank==0):
        for i in range(size):
            listOfLabels.append(recvbuf[i])
        print(listOfLabels)
LOL=np.array(listOfLabels)
print(LOL)
np.savetxt('LOL.csv',LOL,delimiter=',')
