import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product


batch_size=1000
epochs=1000
numReplicates=50
augmentations=[0,0.33,0.66,1,2,5,10]
numCombos=7**9
unlabeledListOfIPs=list(product(augmentations,repeat=10))
maxIters=10
#print(listOfIPs)
#print(type(listOfIPs))
#print(len(listOfIPs))
def getModel():
    model=Sequential()
    model.add(Dense(81, activation='sigmoid', input_shape=(9,)))
    model.add(Dropout(0.2))
    model.add(Dense(81, activation='sigmoid'))
    model.add(Dropout(0.2))
    model.add(Dense(4, activation='linear'))
    model.summary()
    model.compile(loss='mse',optimizer='adam',metrics=['accuracy'])
    return model

startList=np.load('startList.npy')
startLabels=np.load('startLabels.npy')

x_train=startList[0:899,:]
y_train=startLabels[0:899,:]
x_test=startList[900:999,:]
y_test=startList[900:999,:]
labels=[]
labeledListOfIPs=[]
for i in range(batch_size):
    labeledListOfIPs.append(startList[i,:])
    labels.append(startLabels[i,:])

NN=getModel()

history = NN.fit(x_train, y_train,batch_size=batch_size,
                    epochs=epochs,verbose=1)
score = NN.evaluate(x_test, y_test, verbose=0)

def findALRanks(unlabeledListOfIPs,numReplicates,NN):
    data=np.empty([len(unlabeledListOfIPs),numReplicates,4],dtype=np.float32)
    for i in range(len(unlabeledListOfIPs)):
        for j in range(numReplicates):
            data[i,j,:]=NN.predict(unlabeledListOfIPs[i,:])

            listOfVariances=np.empty(len(unlabeledListOfIPs),2)
    for i in range(len(unlabeledListOfIPs)):
        listOfVariances[i,0]=np.std(data[i,0,:])
        listOfVariances[i,1]=np.std(data[i,1,:])+np.std(data[i,1,:])+np.std(data[i,2,:])
        m1=np.amax(listOfVariances[:,0])
        m2=np.amax(listOfVariances[:,1])
        normalizedVariance=np.empty([len(unlabeledListOfIPs)],dtype=np.float32)
    for i in range(len(unlabeledListOfIPs)):
        normalizedVariance[i]=listOfVariances[i,0]/m1+listOfVariances[i,1]/m2
    indexes=np.argsort(normalizedVariance)
    selectedIndexes=indexes[len(unlabeledListOfIPs)-batch_size:len(unlabeledListOfIPs)]
    return selectedIndexes

for X in range(maxIters):
    nextSet=findALRanks(unlabeledListOfIPs,numReplicates,NN)
#####################################################
##  GET NEW LABELS USING AL
####################################################
    for i in range(batch_size):
        temp=unlabeledListOfIPs[nextSet[i]]
        labeledListOfIPs.append(temp)
        unlabeledListOfIPs.del(i)
        labels.append(ANSWERS[i])
    ## ADD NEW DATA TO TRAIN/TEST SET
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                    epochs=epochs,verbose=1)
    score = NN.evaluate(x_test, y_test, verbose=0)











# print('Test loss:', score[0])
# print('Test accuracy:', score[1])
