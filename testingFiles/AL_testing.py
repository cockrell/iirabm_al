import sklearn
import pickle
import numpy as np
import csv

injNum=np.array([1,40])
NIR=np.array([1,2,3,4])
IS=np.array([1,2,3,4,5,6,7,8,9,10])
# NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
NRI=2
oxyHeal=np.linspace(0.05,1,20)

ld=pickle.load(open('labeledData.pkl','rb'))
uld=pickle.load(open('unlabeledData.pkl','rb'))
model=pickle.load(open('RF_modelTest.pkl','rb'))
lol=pickle.load(open('listOfLabels.pkl','rb'))

data=[0,0,0,0]
index=0;
iteration=0;
for i in range(4):
    for j in range(10):
        for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1

data=np.delete(data,0,0)

predictions=np.empty(800)
for i in range(800):
    predictions[i]=model.predict([data[i,:]])

def getCenterPoint(predictions):
    pt1=[]
    pt2=[]
    pt3=[]
    vol=0
    ans1=0
    ans2=0
    ans3=0
    for i in range(800):
        if(predictions[i]==1):
            pt1.append(data[i,0])
            pt2.append(data[i,1])
            pt3.append(data[i,2])
            vol=vol+1
    if(vol!=0):
        ans1=(np.amax(pt1)-np.amin(pt1))/2
        ans2=(np.amax(pt2)-np.amin(pt2))/2
        ans3=(np.amax(pt3)-np.amin(pt3))/2
    answer=[vol,ans1,ans2,ans3]
    return answer

newData=np.empty([275,3])
k=0
for i in range(800):
    if(predictions[i]==1):
        newData[k,:]=data[i,0:3]
        k=k+1

import numpy as np
injNum=np.array([1,40])
NIR=np.array([1,2,3,4])
IS=np.array([1,2,3,4,5,6,7,8,9,10])
# NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
NRI=2
oxyHeal=np.linspace(0.05,1,20)
data=[0,0,0,0]
index=0;
iteration=0;
for i in range(4):
    for j in range(10):
        for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1

data=np.delete(data,0,0)
LOL=np.genfromtxt('LOL_10.csv',delimiter=',')
newData=np.empty([int(np.sum(LOL)),3])
k=0
for i in range(800):
    if(LOL[i]==1):
        newData[k,:]=data[i,0:3]
        k=k+1

np.savetxt('testData_10.csv',newData,delimiter=',')
