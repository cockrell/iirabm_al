import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import numpy as np
from keras.models import load_model

def getModel():
    model=Sequential()
    model.add(Dense(4, activation='sigmoid', input_shape=(4,)))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy',optimizer='adam'  ,metrics=['accuracy'])
    return model

def getData():
    injNum=np.array([1,40])
    NIR=np.array([1,2,3,4])
    IS=np.array([1,2,3,4,5,6,7,8,9,10])
    NRI=2
    oxyHeal=np.linspace(0.05,1,20)
    data=[0,0,0,0]
    index=0;
    iteration=0;
    for i in range(4):
        for j in range(10):
            for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1
    data=np.delete(data,0,0)
    return data

L1=np.zeros([4,4],dtype=np.float32)
L2=np.zeros(4,dtype=np.float32)
L3=np.zeros([4,1],dtype=np.float32)
L4=np.zeros([1],dtype=np.float32)

test=np.load('WeightTest.npy')
testNN1=load_model('modelTest.h5')
LON1=[]
LON2=[]

print(test)

k=0
for i in range(4):
    for j in range(4):
        L1[i,j]=test[k]
        k=k+1
for i in range(4):
    L2[i]=test[k]
    k=k+1
for i in range(4):
    L3[i,0]=test[k]
    k=k+1
L4=test[k]
L4=np.array(L4)
L4.shape=(1,)
print(L4)
print(type(L4))
print(L4.shape)
print("L4=",L4)
LON1.append(L1)
LON1.append(L2)
LON2.append(L3)
LON2.append(L4)


print(type(LON1))
for item in LON1:
    print(type(item),item.shape)
print(type(LON2))
for item in LON2:
    print(type(item),item.shape)

testNN2=getModel()

testNN2.layers[0].set_weights(LON1)
testNN2.layers[1].set_weights(LON2)


data=getData()


t1=testNN1.predict(data)
t2=testNN2.predict(data)

for i in range(800):
    print(t1[i],t2[i])
# for layer in testNN2.layers
# for layer in NN.layers:
#
#     k=k+1
#     print(k)
#     print(layer.get_weights())
#     tempweight=layer.get_weights()
#     for j1 in range(len(tempweight)):
#         testL=np.array(tempweight[j1])
#         testL=testL.ravel()
#         for j2 in range(testL.shape[0]):
#             W[j4]=testL[j2]
#             j4=j4+1
