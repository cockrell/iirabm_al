import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product
import pickle

batch_size=100
epochs=10000
numReplicates=50
augmentations=[0,0.33,0.66,1,2,5,10]
numCombos=7**9
maxIters=10

def getModel():
    keras.backend.clear_session()
    model=Sequential()
    model.add(Dense(256, activation='relu', input_shape=(9,)))  #Experiment with relu
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(3, activation='linear'))
    model.summary()
    model.compile(loss='mse',optimizer='adam',metrics=['accuracy'])
    return model

def getStartData():
    trainData=np.load('InternalParameterization.npy')
    trainAnswers=np.load('TrainCenterVolumes.npy')
    x_train=trainData[0:1800,:]
    y_train=trainAnswers[0:1800,1:4]
    x_test=trainData[1800:1900,:]
    y_test=trainAnswers[1800:1900,1:4]
    return x_train,y_train,x_test,y_test

def findALRanks(unlabeledIndexes,numReplicates,NN):
    data=np.empty([len(unlabeledIndexes),numReplicates],dtype=np.float32)
    for i in range(len(unlabeledIndexes)):
        for j in range(numReplicates):
            data[i,j,:]=NN.predict(listOfIPs[unlabeledIndexes[i],:])
            listOfVariances=np.empty(len(unlabeledListOfIPs),2)
    for i in range(len(unlabeledIndexes)):
        listOfVariances[i,0]=np.std(data[i,0,:])
        listOfVariances[i,1]=np.std(data[i,1,:])+np.std(data[i,1,:])+np.std(data[i,2,:])
        m1=np.amax(listOfVariances[:,0])
        m2=np.amax(listOfVariances[:,1])
        normalizedVariance=np.empty([len(unlabeledIndexes)],dtype=np.float32)
    for i in range(len(unlabeledListOfIPs)):
        normalizedVariance[i]=listOfVariances[i,0]/m1+listOfVariances[i,1]/m2
    indexes=np.argsort(normalizedVariance)
    selectedIndexes=indexes[len(unlabeledListOfIPs)-batch_size:len(unlabeledIndexes)]
    return selectedIndexes

def run(q):
    testData=[]
    x_train,y_train,x_test,y_test=getStartData();
    NN=getModel()
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                    epochs=epochs,verbose=0)
    score = NN.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    NN.save('model_center1_%s.h5'%q)
#     testAnswers=NN.predict(x_test)
#     testAnswers=np.asarray(testAnswers,dtype=np.float32)
#     for i in range(100):
# #        print(testAnswers[i,:],y_test[i,:],abs(testAnswers[i,:]-y_test[i,:]))
#         tempData=abs(testAnswers[i,:]-y_test[i,:])
# #        print(tempData)
#         testData.append([testAnswers[i,0],testAnswers[i,1],testAnswers[i,2],y_test[i,0],y_test[i,1],y_test[i,2],tempData[0],tempData[1],tempData[2]])
#     testData=np.asarray(testData)
# #    print(testData.shape)
#     np.save('testDataCentroid1.npy',testData)
for i in range(15):
    run(i)
