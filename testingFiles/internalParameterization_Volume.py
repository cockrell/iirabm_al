import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from itertools import product
import keras.backend as K
import os
from keras.models import load_model


batch_size=150
epochs=15000
numReplicates=10
augmentations=[0,0.33,0.66,1,2,5,10]
numCombos=7**9
maxIters=10



def predict_with_uncertainty(f, x, n_iter=10):
    result=[]

    for iter in range(n_iter):
        print(iter)
        result.append(f([x, 1]))
        # temp=f([x,1])
        # temp=np.asarray(temp)
        # print(temp.shape)
        # print(temp)
    result=np.asarray(result)
    result.shape=(n_iter,x.shape[0])
    print(result.shape)
#    print(result)
    prediction = result.mean(axis=0)
    uncertainty = result.var(axis=0)
#    np.save('resulttest.npy',result)
    return prediction, uncertainty

def getModel():
    keras.backend.clear_session()
    model=Sequential()
    model.add(Dense(256, activation='relu', input_shape=(9,)))  #Experiment with relu
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear'))
    model.summary()
    model.compile(loss='mse',optimizer='adam',metrics=['accuracy'])
    return model

def getStartData():
    trainData=np.load('InternalParameterization.npy')
    trainAnswers=np.load('TrainCenterVolumes.npy')
    x_train=trainData[0:100,:]
    y_train=trainAnswers[0:100,0]
    x_test=trainData[1800:1900,:]
    y_test=trainAnswers[1800:1900,0]
    TD2=np.load('InternalParameterization_run1.npy')
    TA2=np.load('TrainCenterVolumes_run1.npy')
    x_train=np.vstack((x_train,TD2))
    print(y_train.shape)
    print(TA2.shape)
    y_train=np.concatenate((y_train,TA2[:,0]))
    return x_train,y_train,x_test,y_test

def findALRanks(numCombos,numReplicates,NN):

    data=np.empty([numCombos,numReplicates],dtype=np.float32)
    for i in range(numReplicates):
        data[:,i]=NN.predict(trainData)





    # for i in range(len(unlabeledIndexes)):
    #     for j in range(numReplicates):
    #         data[i,j,:]=NN.predict(listOfIPs[unlabeledIndexes[i],:])
    #         listOfVariances=np.empty(len(unlabeledListOfIPs),1)
    # for i in range(len(unlabeledIndexes)):
    #     listOfVariances[i,0]=np.std(data[i,0,:])
    #     listOfVariances[i,1]=np.std(data[i,1,:])+np.std(data[i,1,:])+np.std(data[i,2,:])
    #     m1=np.amax(listOfVariances[:,0])
    #     m2=np.amax(listOfVariances[:,1])
    #     normalizedVariance=np.empty([len(unlabeledIndexes)],dtype=np.float32)
    # for i in range(len(unlabeledListOfIPs)):
    #     normalizedVariance[i]=listOfVariances[i,0]/m1+listOfVariances[i,1]/m2
    # indexes=np.argsort(normalizedVariance)
    # selectedIndexes=indexes[len(unlabeledListOfIPs)-batch_size:len(unlabeledIndexes)]
    # return selectedIndexes

def run(q):
    testAnswers=[]
    x_train,y_train,x_test,y_test=getStartData();
    NN=getModel()
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                    epochs=epochs,verbose=0)
    score = NN.evaluate(x_test, y_test, verbose=0)
    # f = K.function([NN.layers[0].input, K.learning_phase()],[NN.layers[-1].output])
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    NN.save('model_volume150_%s.h5'%q)
    # predictData=np.load('IPlist_full.npy')
#    predictData=predictData[0:100,:]
    # predictedParameters,predictedUncertainties=predict_with_uncertainty(f,predictData)
    # np.save('PP.npy',predictedParameters)
    # np.save('PU.npy',predictedUncertainties)
    # findALRanks(numCombos,numReplicates,NN)
    # for i in range(100):
    #     print(x_test[i,:])
    # testAnswers=NN.predict(x_test)
    # testAnswers=np.asarray(testAnswers,dtype=np.float32)
    # testData=[]
    # for i in range(100):
    #     testData.append([testAnswers[i],y_test[i],abs(testAnswers[i]-y_test[i]),abs(testAnswers[i]-y_test[i])/y_test[i]])
    # testData=np.asarray(testData)
    # np.save('testData.npy',testData)

for i in range(10):
    run(i)

#run(1)
