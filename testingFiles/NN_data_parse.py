import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import os

batch_size=100
epochs=7500
scoreThreshold=0.9

def getModel():
    keras.backend.clear_session()
    model=Sequential()
    model.add(Dense(4, activation='sigmoid', input_shape=(4,)))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy',optimizer='adam'  ,metrics=['accuracy'])
    return model

def getData():
    injNum=np.array([1,40])
    NIR=np.array([1,2,3,4])
    IS=np.array([1,2,3,4,5,6,7,8,9,10])
    NRI=2
    oxyHeal=np.linspace(0.05,1,20)
    data=[0,0,0,0]
    index=0;
    iteration=0;
    for i in range(4):
        for j in range(10):
            for k in range(20):
                data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                index=index+1
    data=np.delete(data,0,0)
    return data

def getNNweights(internalParameterization,labels):
    data=getData()
    x_train=[]
    y_train=[]
    x_test=[]
    y_test=[]
    indexes=[]
    k=0
    for i in range(800):
        k=k+1
        if(k%4==0):
            x_train.append(data[i,:])
            y_train.append(labels[i])
        else:
            indexes.append(i)
    for i in range(100):
        temp=np.random.randint(0,high=len(indexes))
        x_test.append(data[indexes[temp],:])
        y_test.append(labels[indexes[temp]])
        del indexes[temp]
    x_train=np.asarray(x_train,dtype=np.float32)
    y_train=np.asarray(y_train,dtype=np.float32)
    x_test=np.asarray(x_test,dtype=np.float32)
    y_test=np.asarray(y_test,dtype=np.float32)
    W=refineModel(x_train,x_test,y_train,y_test,indexes,data,labels)
#    keras.backend.clear_session()
#    W=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    print("TestW1",W)
    return W

def getW(NN):
    W=np.empty(25,dtype=np.float32)
    j4=0
    print("TESTNN 2",NN)
    for layer in NN.layers:
        tempweight=layer.get_weights()
        for j1 in range(len(tempweight)):
            testL=np.array(tempweight[j1])
            testL=testL.ravel()
            for j2 in range(testL.shape[0]):
                W[j4]=testL[j2]
                j4=j4+1
    W=np.asarray(W,dtype=np.float32)
    print("W=",W)
    return W

def refineModel(x_train,x_test,y_train,y_test,indexes,data,labels):
    print("Getting Model")
    keras.backend.clear_session()
    NN=getModel()
    print("Model Generated")
    history = NN.fit(x_train, y_train,batch_size=batch_size,
                     epochs=epochs,verbose=0)
    score = NN.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    if((score[1]>=scoreThreshold) or (x_train.shape[0]>=600)):
        W=getW(NN)
        del NN
        keras.backend.clear_session()
        print("TestW2",W)
        return W
    print("training set size=",x_train.shape)
    x_train,y_train,indexes=getNextSamples(NN,x_train,y_train,indexes,data,labels)
    print("training set size=",x_train.shape)
    tempW=refineModel(x_train,x_test,y_train,y_test,indexes,data,labels)
    print("Returning from Recursion",temp420)
    return tempW

def getNextSamples(NN,x_train,y_train,indexes,data,labels):
    probs=[]
    answers=[]
    temp=np.empty([len(indexes),4],dtype=np.float32)
    for i in range(len(indexes)):
        temp[i,:]=data[indexes[i],:]
        answers.append(labels[indexes[i]])
    temp=np.asarray(temp,dtype=np.float32)
    answers=np.asarray(answers,dtype=np.float32)
    probs=NN.predict(temp)
    probs=np.asarray(probs)
    probs=np.squeeze(probs)
    p2=probs
    p2=np.absolute(p2-0.5)
    ind=np.argsort(p2)
    for i in range(50):
        x_train=np.vstack([x_train,data[ind[i],:]])
        y_train=np.append(y_train,labels[ind[i]])
        del indexes[ind[i]-i] # the '-i' essentially re-indexes after removal of an element
    return x_train,y_train,indexes



def run(input,answers):
    k=0
    for i in range(1500):
        for j in range(500):
            file1=str('/home/chase/iirabm_al/Data/IP_%s_%s.csv'%(i,j))
            file2=str('/home/chase/iirabm_al/Data/LOL_%s_%s.csv'%(i,j))
#            print(file1)
            if os.path.isfile(file1):
                internalParameterization=np.genfromtxt(file1,delimiter=',')
                labels=np.genfromtxt(file2,delimiter=',')
                W=getNNweights(internalParameterization,labels)
                print("weights calculated for",i,j)
                print(internalParameterization)
                print(W)
                input[k,:]=internalParameterization
                answers[k,:]=W
                k=k+1
                file3=str('/home/chase/iirabm_al/W_%s_%s.csv'%(i,j))
                np.savetxt(file3,W,delimiter=',')
    return k

input=np.empty([2500,9],dtype=np.float32)
answers=np.empty([2500,25],dtype=np.float32)
k=run(input,answers)
input=input[0:k,:]
answers=answers[0:k,:]
np.save('Input.npy',input)
np.save('Answers.npy',answers)
