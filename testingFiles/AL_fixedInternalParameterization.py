import ctypes
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from mpi4py import MPI
from sklearn.model_selection import cross_val_score
import pickle

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
_IIRABM=ctypes.CDLL('/home/chase/iirabm_al/testABM.so')
_IIRABM.mainSimulation.argtypes=(ctypes.c_float,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.POINTER(ctypes.c_float))
# (int inj_number, float oxyHeal, int infectSpread, int numRecurInj, int numInfectRepeat, int seed)

accCutoff=0.9
numEstimators=30

#Gets a class (clinically relevant=1 or not=0) for a given external parameterization
def getClass(NIR,IS,NRI,oxyHeal,internalParameterization):
    answer=0
    c_float_p = ctypes.POINTER(ctypes.c_float)
    NIR=ctypes.c_int(NIR)
    IS=ctypes.c_int(IS)
    NRI=ctypes.c_int(NRI)
    oxyHeal=ctypes.c_float(oxyHeal)
    answer=_IIRABM.mainSimulation(oxyHeal,IS,NRI,NIR,9,internalParameterization.ctypes.data_as(c_float_p))
    print(color,newRank,answer)
    print(oxyHeal,IS,NIR)
    return answer

def createModel(trainData,trainAnswers):
    print(trainData)
    print(trainAnswers)
    clf = RandomForestClassifier(n_estimators=numEstimators,random_state=0)
    clf.fit(trainData,trainAnswers)
    return clf

def getScores(rfModel,trainData,trainAnswers):
    scores = cross_val_score(rfModel, trainData, trainAnswers, cv=5)
    print(scores)
    return(scores.mean())

def iterateModel(count,internalParameterization,sList,data,unlabeled,labeled,listOfLabels,trainData,trainAnswers):
    comm.Barrier()
    if rank==0:
        print("Starting Iteration",count)
        sendList=[]
        if(count==0):
            for i in range(size):
                temp=np.random.randint(0,high=len(unlabeled))
                value=unlabeled[temp]
                unlabeled.remove(value)
                sendList.append(value)
                sendListN=np.array(sendList)
        else:
            sendListN=np.array(sList)
            sendList=sList
    sendbuf=[]
    if rank==0:
        sendbuf = sendListN
        print(sendListN)
    recvbuf=np.empty([1,1],dtype=np.int64)
    comm.Scatter(sendbuf, recvbuf, root=0)

    answer=getClass(data[recvbuf,0],data[recvbuf,1],data[recvbuf,3],data[recvbuf,2],internalParameterization)
    recvbuf=None
    sendbuf=np.empty(1,dtype=np.int64)
    sendbuf[0]=answer
    if rank==0:
        recvbuf=np.empty([size],dtype=np.int64)
    comm.Gather(sendbuf,recvbuf,root=0)
    recvCV=np.empty(1,dtype=np.float32)
    if(rank==0):
        for i in range(len(sendList)):
            labeled.append(sendList[i])
            listOfLabels.append(recvbuf[i])
            trainData.append(data[sendList[i],:])
            trainAnswers.append(recvbuf[i])
        rfModel=createModel(trainData,trainAnswers)
        avgCV=getScores(rfModel,trainData,trainAnswers)
        avgCV=np.float32(avgCV)
        recvCV=avgCV
    comm.Bcast(recvCV,root=0)
    if(rank==0):
        nextSamples=chooseNextSamples(rfModel,unlabeled,data,size)
        tempCount=0
        sendList=[]
        for i in range(size):
            sendList.append(nextSamples[i])
            del unlabeled[nextSamples[i]-tempCount]
            tempCount=tempCount+1
    comm.Barrier()
    if(recvCV>accCutoff and len(trainData)<(800-size)):
        if(rank==0):
            stats=getCenterPoint(rfModel,data)
            print("Run Complete ",stats)
    else:
        count=count+1
        if(rank!=0):
            sendList=[]
        iterateModel(count,internalParameterization,sendList,data,unlabeled,labeled,listOfLabels,trainData,trainAnswers)

def getCenterPoint(model,data):
    pt1=[]
    pt2=[]
    pt3=[]
    vol=0
    ans1=0
    ans2=0
    ans3=0
    for i in range(800):
        if(model.predict([data[i,:]])==1):
            pt1.append(data[i,0])
            pt2.append(data[i,1])
            pt3.append(data[i,2])
            vol=vol+1
    if(vol!=0):
        pt1=np.asarray(pt1)
        pt2=np.asarray(pt2)
        pt3=np.asarray(pt3)
        ans1=np.sum(pt1)/vol
        ans2=np.sum(pt2)/vol
        ans3=np.sum(pt3)/vol
    answer=[vol,ans1,ans2,ans3]
    return answer

def chooseNextSamples(rfModel,unlabeled,data,numSamples):
    probs=[]
    for i in range(len(unlabeled)):
        probs.append(rfModel.predict_proba([data[unlabeled[i],:]]))
    probs=np.asarray(probs)
    probs=np.squeeze(probs)
    p2=probs[:,0]
    p2=np.absolute(p2-0.5)
    indexes=np.argsort(p2)
    indexes=indexes[0:numSamples]
    indexes=np.sort(indexes)
    return indexes

def run(internalParameterization):
    np.random.seed(0)
    samplesPerIter=10
    procsPerAL=20
    injNum=np.array([1,40])
    NIR=np.array([1,2,3,4])
    IS=np.array([1,2,3,4,5,6,7,8,9,10])
    # NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
    NRI=2
    oxyHeal=np.linspace(0.05,1,20)
    trainData=[]
    trainAnswers=[]
    internalParameterization=np.asarray(internalParameterization,dtype=np.float32)

    data=[0,0,0,0]
    index=0;
    iteration=0;
    for i in range(4):
        for j in range(10):
            for k in range(20):
                    data=np.vstack([data,[NIR[i],IS[j],oxyHeal[k],NRI]])
                    index=index+1
    data=np.delete(data,0,0)
    unlabeled=[]
    labeled=[]
    listOfLabels=[]
    for i in range(data.shape[0]):
        unlabeled.append(i)
    blankList=[]
    iterateModel(iteration,internalParameterization,blankList,data,unlabeled,labeled,listOfLabels,trainData,trainAnswers)

internalParameterization=np.array([1,1,1,1,1,1,1,1,1],dtype=np.float32)
run(internalParameterization)
