import ctypes
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from mpi4py import MPI
from sklearn.model_selection import cross_val_score
import pickle

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
_IIRABM=ctypes.CDLL('/home/chase/iirabm_al/testABM.so')
_IIRABM.mainSimulation.argtypes=(ctypes.c_float,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.c_int,ctypes.POINTER(ctypes.c_float))
internalParameterization=np.array([1,1,1,1,1,1,1,1,1],dtype=np.float32)
c_float_p = ctypes.POINTER(ctypes.c_float)
temp=_IIRABM.mainSimulation(0.2,9,2,4,0,9,internalParameterization.ctypes.data_as(c_float_p))
print(temp)
temp=_IIRABM.mainSimulation(0.4,9,2,4,0,9,internalParameterization.ctypes.data_as(c_float_p))
print(temp)
temp=_IIRABM.mainSimulation(0.9,4,2,3,0,9,internalParameterization.ctypes.data_as(c_float_p))
print(temp)
# def mainSimulation(injSize,oxyHeal,infectSpread,numRecurInj,numInfectRepeat,seed):
#   global _fitness
#   result=_fitness.mainSimulation(ctypes.c_int(injSize),ctypes.c_float(oxyHeal),ctypes.c_int(infectSpread),ctypes.c_int(numRecurInj),ctypes.c_int(numInfectRepeat),ctypes.c_int(seed))
#   x=5
#   return int(result)
