import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import numpy as np
from keras.models import load_model
import os


def getStartData():
    trainData = np.load('LabeledInternalParameterizations.npy')
    trainAnswers = np.load('TrainCenterVolumes.npy')
    trainAnswers = trainAnswers[:, 1:4]
    testData = trainData[4086:4186, :]
    testAnswers = trainAnswers[4086:4186,:]
    return trainData, trainAnswers, testData, testAnswers

fullData, fullAnswers, testData, testAnswers = getStartData()
testVolumes = np.zeros([250, 4186], dtype=np.float32)
for i in range(250):
    filename1 = str('center_volumeAL_start50_select5_%s_0.h5' % i)
    filename2 = str('center_volumeAL_start50_select5_%s_1.h5' % i)
    filename3 = str('center_volumeAL_start50_select5_%s_2.h5' % i)

    keras.backend.clear_session()
    NN = load_model(filename1)
    X1 = NN.predict(fullData)
    keras.backend.clear_session()
    NN = load_model(filename2)
    X2 = NN.predict(fullData)
    keras.backend.clear_session()
    NN = load_model(filename3)
    X3 = NN.predict(fullData)
    for j in range(4186):
        # t1 = abs(X1[j,0]-fullAnswers[j,0])/fullAnswers[j,0]+abs(X1[j,1]-fullAnswers[j,1])/fullAnswers[j,1]+abs(X1[j,2]-fullAnswers[j,2])/fullAnswers[j,2]
        # t2 = abs(X2[j,0]-fullAnswers[j,0])/fullAnswers[j,0]+abs(X2[j,1]-fullAnswers[j,1])/fullAnswers[j,1]+abs(X2[j,2]-fullAnswers[j,2])/fullAnswers[j,2]
        # t3 = abs(X3[j,0]-fullAnswers[j,0])/fullAnswers[j,0]+abs(X3[j,1]-fullAnswers[j,1])/fullAnswers[j,1]+abs(X3[j,2]-fullAnswers[j,2])/fullAnswers[j,2]
        t1 = abs(X1[j,0]-fullAnswers[j,0])+abs(X1[j,1]-fullAnswers[j,1])+abs(X1[j,2]-fullAnswers[j,2])
        t2 = abs(X2[j,0]-fullAnswers[j,0])+abs(X2[j,1]-fullAnswers[j,1])+abs(X2[j,2]-fullAnswers[j,2])
        t3 = abs(X3[j,0]-fullAnswers[j,0])+abs(X3[j,1]-fullAnswers[j,1])+abs(X3[j,2]-fullAnswers[j,2])
        t11 = (t1+t2+t3)/3
        testVolumes[i, j] = t11

np.savetxt('TCA_start50_select5.csv', testVolumes, delimiter=',')
STV = np.mean(testVolumes, axis=1)
np.savetxt('SCA_start50_select5.csv', STV, delimiter=',')
