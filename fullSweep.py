import ctypes
import numpy as np
from mpi4py import MPI
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
_IIRABM = ctypes.CDLL('/home/chase/iirabm_al/testABM.so')
_IIRABM.mainSimulation.argtypes = (
    ctypes.c_float, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
    ctypes.POINTER(ctypes.c_float))

injNum = np.array([1, 40])
NIR = np.array([1, 2, 3, 4])
IS = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
# NRI=np.array[0,1,2,3,4,5,6,7,8,9,10]
internalParameterization = np.array(
    [1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=np.float32)

NRI = 2
oxyHeal = np.linspace(0.05, 1, 20)

data = [0, 0, 0, 0]
index = 0
iteration = 0
for i in range(4):
    for j in range(10):
        for k in range(20):
            data = np.vstack([data, [NIR[i], IS[j], oxyHeal[k], NRI]])
            index = index+1
data = np.delete(data, 0, 0)


def getClass(NIR, IS, NRI, oxyHeal, internalParameterization):
    answer = 0
    c_float_p = ctypes.POINTER(ctypes.c_float)
    NIR = int(NIR)
    NRI = int(NIR)
    IS = int(IS)
    NIR = ctypes.c_int(NIR)
    IS = ctypes.c_int(IS)
    NRI = ctypes.c_int(NRI)
    oxyHeal = ctypes.c_float(oxyHeal)
    answer = _IIRABM.mainSimulation(
        oxyHeal, IS, NRI, NIR, 9,
        internalParameterization.ctypes.data_as(c_float_p))
    return answer


listOfLabels = []
# IPlist=np.load('IPlist_full.npy')
IPlist = np.load('IPlist_AL1.npy')
seed = int(sys.argv[1])
np.random.seed(seed)
for q in range(100):
    # temp=np.random.randint(0,high=7**9)
    # internalParameterization=IPlist[temp,:]
    internalParameterization = IPlist[q, :]
    numcount = int(800/size)
    for count in range(numcount):
        if(rank == 0):
            print("IP Iteration=", q, " internal Iteration=", count)
        comm.Barrier()
        answer = getClass(data[rank+count*size, 0], data[rank+count*size, 1],
                          data[rank+count*size, 3], data[rank+count*size, 2],
                          internalParameterization)
        recvbuf = None
        sendbuf = np.empty(1, dtype=np.int64)
        sendbuf[0] = answer
        if rank == 0:
            recvbuf = np.empty([size], dtype=np.int64)
        comm.Gather(sendbuf, recvbuf, root=0)
        if(rank == 0):
            for i in range(size):
                listOfLabels.append(recvbuf[i])
    if(rank == 0):
        LOL = np.array(listOfLabels[800*q:800*(q+1)])
        filename = str('LOL_%s_%s.csv' % (seed, q))
        filename2 = str('IP_%s_%s.csv' % (seed, q))
        np.savetxt(filename, LOL, delimiter=',')
        np.savetxt(filename2, internalParameterization, delimiter=',')
