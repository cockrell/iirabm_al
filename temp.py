z=0
flag=0
answers=np.empty([5,15],dtype=np.float32)
for j in range(500):
    for k in range(16):
        filename=str('model_volume%s_%s.h5'%(j,k))
        if os.path.isfile(filename):
            flag++
            x_train,y_train,x_test,y_test=getStartDataV()
            NN=getModel(filename)
            testAnswers=returnPredictions(NN,x_test)
            testData=[]
            for i in range(100):
                testData.append([testAnswers[i],y_test[i],abs(testAnswers[i]-y_test[i]),abs(testAnswers[i]-y_test[i])/y_test[i]])
            testData=np.asarray(testData)
            answers[z,k]=np.mean(testData[:,3])
    if(flag==9):
        z++
        flag=0
np.save('volAnswers.npy',answers)
