import numpy as np

variances = np.load('PredictedUncertainties_200_1.npy')
IPL = np.load('IPlist_full.npy')
size = variances.shape[0]

meanVariances = np.mean(variances, axis=1)

print("Test 3", meanVariances.shape)

sortedIndex = np.argsort(meanVariances)
sortedIndex = np.flip(sortedIndex)
selectedIndexes = sortedIndex[0:100]
selectedParams = np.empty([100, 9], dtype=np.float32)
for i in range(100):
    selectedParams[i, :] = IPL[selectedIndexes[i], :]


np.save('SelectedIndexes_1.npy', selectedIndexes)
np.save('SelectedParams_1.npy', selectedParams)
