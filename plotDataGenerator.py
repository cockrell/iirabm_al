import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import os
from keras.models import load_model


def getModel(filename):
    print(filename)
    keras.backend.clear_session()
    NN = load_model(filename)
    return NN


def getStartDataV():
    trainData = np.load('InternalParameterization.npy')
    trainAnswers = np.load('TrainCenterVolumes.npy')
    x_train = trainData[0:1800, :]
    y_train = trainAnswers[0:1800, 0]
    x_test = trainData[1800:1900, :]
    y_test = trainAnswers[1800:1900, 0]
    return x_train, y_train, x_test, y_test


def getStartDataC():
    trainData = np.load('InternalParameterization.npy')
    trainAnswers = np.load('TrainCenterVolumes.npy')
    x_train = trainData[0:1800, :]
    y_train = trainAnswers[0:1800, 1:4]
    x_test = trainData[1800:1900, :]
    y_test = trainAnswers[1800:1900, 1:4]
    return x_train, y_train, x_test, y_test


def returnPredictions(NN, x_test):
    temp = NN.predict(x_test)
    return temp


answers = np.empty([5, 15], dtype=np.float32)
for j in range(6):
    for k in range(16):
        filename = str('model_volume%s_%s.h5' % (j, k))
        if os.path.isfile(filename):
            x_train, y_train, x_test, y_test = getStartDataV()
            NN = getModel(filename)
            testAnswers = returnPredictions(NN, x_test)
            testData = []
            for i in range(100):
                testData.append([testAnswers[i], y_test[i], abs(
                    testAnswers[i]-y_test[i]), abs(testAnswers[i]-y_test[i])/y_test[i]])
            testData = np.asarray(testData)
            answers[j-1, k] = np.mean(testData[:, 3])
np.save('volAnswers.npy', answers)

canswers = np.empty([5, 15, 3], dtype=np.float32)
for j in range(6):
    for k in range(16):
        filename = str('model_center%s_%s.h5' % (j, k))
        if os.path.isfile(filename):
            x_train, y_train, x_test, y_test = getStartDataC()
            NN = getModel(filename)
            testAnswers = returnPredictions(NN, x_test)
            testData = []
            for i in range(100):
                tempData = abs(testAnswers[i, :]-y_test[i, :])
                testData.append([testAnswers[i, 0], testAnswers[i, 1], testAnswers[i, 2], y_test[i, 0], y_test[i, 1], y_test[i, 2],tempData[0],tempData[1],tempData[2]])
            testData = np.asarray(testData)
            canswers[j-1, k, 0] = np.mean(testData[:, 6])
            canswers[j-1, k, 1] = np.mean(testData[:, 7])
            canswers[j-1, k, 2] = np.mean(testData[:, 8])
np.save('centerAnswers.npy', canswers)

V = np.empty(15)
C = np.empty([15, 3])
Vs = np.empty(15)
Cs = np.empty([15, 3])
for i in range(15):
    V[i] = np.mean(answers[:, i])
    C[i, 0] = np.mean(canswers[:, i, 0])
    C[i, 1] = np.mean(canswers[:, i, 1])
    C[i, 2] = np.mean(canswers[:, i, 2])
    Vs[i] = np.std(answers[:, i])
    Cs[i, 0] = np.std(canswers[:, i, 0])
    Cs[i, 1] = np.std(canswers[:, i, 1])
    Cs[i, 2] = np.std(canswers[:, i, 2])

V
C
