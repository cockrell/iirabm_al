import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
import os
from keras.models import load_model
import keras.backend as K

keras.backend.clear_session()
NN = load_model('NN_ParamModel_200start_dropout.h5')
IPL = np.load('IPlist_full.npy')

# predict_with_uncertainty from https://stackoverflow.com/questions/43529931/how-to-calculate-prediction-uncertainty-using-keras
f = K.function([NN.layers[0].input, K.learning_phase()],
               [NN.layers[-1].output])


def predict_with_uncertainty(f, x, n_iter=10):
    result = []

    for iter in range(n_iter):
        result.append(f([x, 1]))
        # temp=f([x,1])
        # temp=np.asarray(temp)
        # print(temp.shape)
        # print(temp)
    result = np.asarray(result)
    result.shape = (10, 10000, 25)
    prediction = result.mean(axis=0)
    uncertainty = result.var(axis=0)
#    np.save('resulttest.npy',result)
    return prediction, uncertainty


# predictedParameters=np.empty([IPL.shape[0],25,2],dtype=np.float32)

print("Data Loaded--------------------------------------------------", IPL.shape)
# for i in range(5):
#     predictedParameters[:,:,i]=NN.predict(IPL[0:100,:],verbose=0)

PP = np.zeros([4035*10000, 25])
PU = np.zeros([4035*10000, 25])
for i in range(4035):
    print(i)
    test = IPL[i*10000:(i+1)*10000, :]
    predictedParameters, predictedUncertainties = predict_with_uncertainty(
        f, test)
    PP[i*10000:(i+1)*10000, :] = predictedParameters
    PU[i*10000:(i+1)*10000, :] = predictedUncertainties
#    print(predictedUncertainties)
#    print(PU[i*10000:(i+1)*10000,:])
# np.save('PredictedParams_200_1.npy',PP)
np.save('PredictedUncertainties_200_1.npy', PU)
